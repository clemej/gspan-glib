/* 
 * extend.h: 
 *
 * Author: John Clemens <clemej1 at umbc.edu>
 * Copyrigt (c) 2015
 */
#ifndef __EXTEND_H__
#define __EXTEND_H__

void enumerate(struct gspan *gs, GList *projections, GList *right_most_path,
			GHashTable *pm_backward, GHashTable *pm_forward, 
			int min_label);

#endif
