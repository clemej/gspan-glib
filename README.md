A Simple C (with Glib) implementation of gSpan
==============================================

See: http://www.deater.net/john/pages/gspan-implementations.html

Based off of the python version here:
https://gitlab.com/clemej/pygspan

Which is in turn based off the gSpan implementation found here:
https://github.com/Jokeren/DataMining-gSpan

See: http://www.deater.net/john/pages/gspan-implementations.html

Todo:
=====

I'm not actively working on this, but replacing some of the 
GList linear scanning with GQueues and GArrays should provide
significant speedups.  In fact, I had some code that did this
but had to remove it as this was for a class project and the
(ancient) version of glib on the target server didn't support
GQueues and GArrays. I would add it back in, but I also don't 
remember if I fully debugged it yet or not, so I'm going to 
leave this version as-is.



